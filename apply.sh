#!/bin/bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Bash strict-mode
set -o errexit
set -o nounset
#set -o pipefail

environment="${1:-}"

if [ -z "${environment}" ]; then
  echo "You must provide an 'environment' name argument"
  exit 1
fi

JOB_NAME="${USERNAME}-manual-run-001"
NAMESPACE="chat-matrix-${environment}"

tk apply "environments/chat-matrix/${environment}"

echo "#### Run the job ${JOB_NAME} ####"

kubectl delete job "${JOB_NAME}" -n "${NAMESPACE}" || true
kubectl create job -n "${NAMESPACE}" --from=cronjob/chatservice-sync "${JOB_NAME}"

echo "#### Sleep for 1m  ####"
sleep 1m 

JOB_NAME="${USERNAME}-manual-run-002"
echo "#### Run the job ${JOB_NAME} ####"
kubectl delete job "${JOB_NAME}" -n "${NAMESPACE}" || true
kubectl create job -n "${NAMESPACE}" --from=cronjob/chatservice-sync "${JOB_NAME}"
